自動取得最新版本的函數庫並打包為方便瀏覽器載入的檔案。

# 環境需求

* POSIX shell environment
* Node.js
* NPM

# 執行方法

在 Shell 環境下執行以下腳本：

    build/build.sh
